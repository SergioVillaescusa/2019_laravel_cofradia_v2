@extends('layouts.layout_jorge')
@section('contenido')
	<a href="{{ url()->previous()  }}"><button type="button" class="btn btn-outline-dark">Volver</button></a>
<form method="post" action="{{ route('cuotas.store') }}" enctype="multipart/form-data">
	@csrf
  <div class="form-group">
    <div class="form-group col-12">
      <label for="text">Nombre</label>
      <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre y apellidos">
    </div>
    <div class="form-group col-12">
      <label for="text">Cantidad</label>
      <input type="number" class="form-control" id="valor" name="valor" placeholder="Cantidad">
    </div>
  	<div class="form-group col-12">
	    <label for="inputAddress">Concepto</label>
	    <input type="text" class="form-control" id="concepto" name="concepto" placeholder="Pago mes Diciembre">
  	</div>
  	<div class="form-group col-12">
		<label for="inputCity">Fecha Cobro</label>
	    <input type="date" id="fecha_cobro" name="fecha_cobro">
	</div>
	<div class="form-group col-12">
		<label for="inputCity">Subir Imagen</label>
		<input type="file" class="form-control-file" name="imagen" id="imagen">
  </div>
<button type="submit" class="btn btn-primary">Guardar Datos</button>
</form>
@stop	
