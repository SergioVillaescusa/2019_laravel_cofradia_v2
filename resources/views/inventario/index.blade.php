@extends('layout.layout')


	
		
	@section('content')
	<div class="container"> 

		@if(session()->get('success'))
		    <div class="alert alert-success">
		      {{ session()->get('success') }}  
		    </div><br />
		@endif
		<div class="row">
			<table class="table">
				<thead class="thead-dark">
					<tr >
						<th scope="col">ID</th>
						<th scope="col">Codigo</th>
						<th scope="col">Nombre</th>
						<th scope="col">Cantidad</th>
						<th scope="col">Proveedor</th>
						<th scope="col">Precio</th>
						<th scope="col">Disponibilidad</th>
						<th scope="col">Editar</th>
						<th scope="col">Eliminar</th>
						<th scope="col">Mostrar</th>
					</tr>
				</thead>
				<tbody>

					@foreach($listadoInventario as $item)
					<tr>
						<td>{{$item->id}}</td>
						<td>{{$item->codigo}}</td>
						<td>{{$item->nombre}}</td>
						<td>{{$item->cantidad}}</td>
						<td>{{$item->proveedor}}</td>
						<td>{{$item->precio}}€</td>
						<td>{{$item->disponibilidad}}</td>
						<td><a class="btn btn-primary" href="inventario/edit/{{$item->id}}" role="button" >Editar</a></td>
						<td>
							 <form action="{{ route('inventario.destroy', $item->id) }}" method="post">
			                  @csrf
			                  @method('DELETE')
			                  <button class="btn btn-danger" type="submit">Eliminar</button>
			                </form>
						</td>
						<td><a class="btn btn-info" href="inventario/show/{{$item->id}}" role="button" >Mostrar</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<!-- Footer -->
@endsection

