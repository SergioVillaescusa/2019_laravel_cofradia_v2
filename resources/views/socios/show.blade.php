<!DOCTYPE html>
<html>
<head>
	<title>Tabla socios</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>
	<form action="{{ route('socios.update', $socio->id) }}" method="post">
		@method('PUT')
		@csrf
		@extends('layout.layout')

@section('content')
	  <div class="form-row">
	    <div class="form-group col-md-6">
	      <label for="Nombre">Nombre</label>
	      <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $socio->nombre ?>" readonly>
	    </div>
	    <div class="form-group col-md-6">
	      <label for="Apellidos">Apellidos</label>
	      <input type="text" class="form-control" id="apellidos" name="apellidos" value="<?php echo $socio->apellidos ?>" readonly>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="inputAddress">DNI</label>
	    <input type="text" class="form-control" id="dni" name="dni" value="<?php echo $socio->dni ?>" readonly>
	  </div>
	  <div class="form-group">
	    <label for="telefono">Telefono</label>
	    <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $socio->telefono ?>" readonly>
	  </div>
	  <div class="form-row">
	    <div class="form-group col-md-6">
	      <label for="fecha">Fecha de nacimiento</label>
	      <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $socio->fecha_de_nacimiento ?>" readonly>
	    </div>
	    <div class="form-group col-md-3">
	      <label for="inputState">Localidad</label>
	      <input type="text" class="form-control" id="localidad" name="localidad" value="<?php echo $socio->localidad ?>" readonly>
	    </div>
	    <div class="form-group col-md-3">
	      <label for="provincia">Provincia</label>
	      <input type="text" class="form-control" id="provincia" name="provincia" value="<?php echo $socio->provincia ?>" readonly>
	    </div>
	  </div>
	  <div class="form-row">
		  <div class="form-group col-md-12">
		    <label for="direccion">Direccion</label>
		    <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $socio->direccion ?>" readonly>
		  </div>
	  </div>	  
	  <div class="form-row">
		  <div class="form-group col-md-6">
		    <label for="pais">Pais</label>
		    <input type="text" class="form-control" id="pais" name="pais" value="<?php echo $socio->pais ?>" readonly>
		  </div>
		  <div class="form-group col-md-6">
		    <label for="codigo">Codigo Postal</label>
		    <input type="text" class="form-control" id="codigop" name="codigop" value="<?php echo $socio->codigo_postal ?>" readonly>
		  </div>
	  </div>	
	  <div class="form-group">
	    <label for="observaciones">Observaciones</label>
	    <textarea class="form-control" id="observaciones" readonly name="observaciones" rows="3"><?php echo $socio->observaciones ?></textarea>
	  </div>
	  <div class="form-group">
			<label for="imagen">Imagen</label>
            <?php if (!empty($socio->ruta)){ ?>
				<img src="{{ asset ('storage/'.$socio->ruta) }}">
            <?php } ?>
	  </div>

	  <a type="button" class="btn btn-primary" href='{{ route("socios.index") }}'> Volver</a>
@endsection


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>