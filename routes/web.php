<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('tesoreria','TesoreriaController@index')->name('tesoreria.index');

Route::get('tesoreria/create','TesoreriaController@create')->name('tesoreria.create');
Route::post('tesoreria/create','TesoreriaController@store')->name('tesoreria.store');
Route::get('tesoreria/edit/{id}', 'TesoreriaController@edit')->name('tesoreria.edit');
Route::put('tesoreria/update/{id}' , 'TesoreriaController@update')->name('tesoreria.update');
Route::delete('tesoreria/{id}' , 'TesoreriaController@destroy')->name('tesoreria.destroy');
Route::get('tesoreria/show/{id}' , 'TesoreriaController@show')->name('tesoreria.show');

Route::get('socios', 'SociosController@index')->name('socios.index');

Route::get('socios/create', 'SociosController@create')->name('socios.create');

Route::post('socios/create', 'SociosController@store')->name('socios.store');

Route::get('socios/edit/{id}', 'SociosController@edit')->name('socios.edit');

Route::put('socios/update/{id}', 'SociosController@update')->name('socios.update');

Route::delete('socios/destroy{id}', 'SociosController@destroy')->name('socios.destroy');

Route::get('socios/show/{id}', 'SociosController@show')->name('socios.show');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/logout','Auth\LoginController@logout')->name('logout');

//Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout'); // sergio


Route::get('cuotas','CuotasController@index')->name('cuotas.index');
Route::get('cuotas/create','CuotasController@create')->name('cuotas.create');
Route::post('cuotas/create','CuotasController@store')->name('cuotas.store');
Route::get('cuotas/edit/{id}','CuotasController@edit')->name('cuotas.edit');
Route::put('cuotas/update/{id}','CuotasController@update')->name('cuotas.update');
Route::delete('cuotas/destroy/{id}','CuotasController@destroy')->name('cuotas.destroy');
Route::get('cuotas/show/{id}','CuotasController@show')->name('cuotas.show');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cerrarSesion', 'Auth\LoginController@logout')->name('cerrarSesion');

Route::get('/dynamic_pdf', 'DynamicPDFController@index');
Route::get('/dynamic_pdf/pdf', 'DynamicPDFController@pdf');


Route::get('inventario','InventarioController@index')->name('inventario.index');
Route::get('inventario/create','InventarioController@create')->name('inventario.create');
Route::post('inventario/create','InventarioController@store')->name('inventario.store');
Route::get('inventario/edit/{id}','InventarioController@edit')->name('inventario.edit');
Route::put('inventario/update/{id}','InventarioController@update')->name('inventario.update');
Route::delete('inventario/destroy/{id}','InventarioController@destroy')->name('inventario.destroy');
Route::get('inventario/show/{id}','InventarioController@show')->name('inventario.show');

