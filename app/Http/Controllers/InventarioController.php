<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventario;

class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $inventario = Inventario::all();
        return view('inventario.index',['listadoInventario'=>$inventario]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         //$i = Inventario::all();
        return view('inventario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $request->validate([
        'codigo'=>'required',
        'nombre'=> 'required',
        'descripcion' => 'required',
        'cantidad' => 'required',
        'precio' => 'required',
        'proveedor' => 'required',
        'fichero' => 'mimes:jpeg,jpg,png'
      ]);
        $fichero=$request->file('fichero');
        $nombre_fichero=$fichero->getClientOriginalName();
        $extension = pathinfo( $nombre_fichero, PATHINFO_EXTENSION );
        $path='storage';
        $nombre_fichero= uniqid('imagen_',true).$nombre_fichero;
        //if ($extension=="jpg" || $extension=="png" || $extension=="gif"){
            $request->file('fichero')->move($path, $nombre_fichero);
        //}

        

        $inventario = new Inventario([
        'codigo' => $request->get('codigo'),
        'nombre' => $request->get('nombre'),
        'descripcion' => $request->get('descripcion'),
        'cantidad' => $request->get('cantidad'),
        'precio' => $request->get('precio'),
        'proveedor'=> $request->get('proveedor'),
        'ruta'=> $nombre_fichero,
      ]);
      $inventario->save();
       /*if ($extension=="jpg" || $extension=="png" || $extension=="gif"){
            $inventario->ruta = $nombre_fichero;
        }
        else{
            return redirect()->action(
                'InventarioController@create', ['codigo' => $inventario->codigo]
            )->with('status', 'Socio no creado, la imagen tiene que ser .jpg .png o .gif');
        }*/
      return redirect('/inventario')->with('success', 'Se ha agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $inventario = Inventario::find($id);

        return view('inventario.show',['listadoInventario'=>$inventario]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

         $inventario = Inventario::find($id);

        return view('inventario.edit', compact('inventario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
        'codigo'=>'required',
        'nombre'=> 'required',
        'descripcion' => 'required',
        'cantidad' => 'required',
        'precio' => 'required',
        'proveedor' => 'required',
        'fichero' => 'mimes:jpeg,jpg,png'
      ]);
        $fichero=$request->file('fichero');
        $nombre_fichero=$fichero->getClientOriginalName();
        $extension = pathinfo( $nombre_fichero, PATHINFO_EXTENSION );
        $path='storage';
        $nombre_fichero= uniqid('imagen_',true).$nombre_fichero;
        //if ($extension=="jpg" || $extension=="png" || $extension=="gif"){
            $request->file('fichero')->move($path, $nombre_fichero);
        //}

     $inventario = Inventario::find($id);
      $inventario->codigo = $request->get('codigo');
      $inventario->nombre = $request->get('nombre');
      $inventario->descripcion = $request->get('descripcion');
      $inventario->cantidad = $request->get('cantidad');
      $inventario->precio = $request->get('precio');
      $inventario->proveedor = $request->get('proveedor');
      $inventario->ruta= $nombre_fichero;
      /* if ($extension=="jpg" || $extension=="png" || $extension=="gif"){
            $inventario->ruta = $nombre_fichero;
        }
        else{
            return redirect()->action(
                'InventarioController@edit', ['id' => $id]
            )->with('status', 'Socio no actualizado, la imagen tiene que ser .jpg .png o .gif');
        }*/
      $inventario->save();
      return redirect('/inventario')->with('success', 'Se ha agregado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
     $inventario = Inventario::find($id);
     $inventario->delete();

     return redirect('/inventario')->with('success', 'Se ha borrado correctamente');
    }
}
