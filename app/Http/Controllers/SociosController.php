<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Socio;

class SociosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socios = Socio::all();
		return view('socios.index',['listadoSocios'=>$socios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('socios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'nombre'=>'required',
        'apellidos'=> 'required',
        'dni' => 'required',
        'telefono'=>'required',
        'fecha'=> 'required',
        'localidad' => 'required',
        'provincia'=>'required',
        'direccion'=> 'required',
        'pais' => 'required',
        'codigop'=>'required',
        'observaciones'=> 'required',
        'fichero'=>'mimes:jpeg,jpg,png,gif'
      ]);

        $fichero=$request->file("fichero");
        $nombre_fichero=$fichero->getClientOriginalName();
        $nombre_fichero= uniqid("img_",true)."_".$nombre_fichero;
        $path="storage";
        $request->file('fichero')->move($path, $nombre_fichero);

      $socio = new Socio([
        'nombre' => $request->get('nombre'),
        'apellidos'=> $request->get('apellidos'),
        'dni'=> $request->get('dni'),
        'telefono'=> $request->get('telefono'),
        'fecha_de_nacimiento'=> $request->get('fecha'),
        'localidad'=> $request->get('localidad'),
        'provincia'=> $request->get('provincia'),
        'direccion'=> $request->get('direccion'),
        'pais'=> $request->get('pais'),
        'codigo_postal'=> $request->get('codigop'),
        'observaciones'=> $request->get('observaciones'),

      ]);
      $socio->ruta = $nombre_fichero;
      $socio->save();
      return redirect('/socios')->with('success', 'Dado de alta con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $socio = Socio::find($id);
        return view('socios.show', compact('socio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $socio = Socio::find($id);
        return view('socios.edit', compact('socio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'nombre'=>'required',
        'apellidos'=> 'required',
        'dni' => 'required',
        'telefono'=>'required',
        'fecha'=> 'required',
        'localidad' => 'required',
        'provincia'=>'required',
        'direccion'=> 'required',
        'pais' => 'required',
        'codigop'=>'required',
        'observaciones'=> 'required',
        'fichero'=>'mimes:jpeg,jpg,png,gif'

      ]);
        $fichero=$request->file("fichero");
        $nombre_fichero=$fichero->getClientOriginalName();
        $nombre_fichero= uniqid("img_",true)."_".$nombre_fichero;

        $path="storage";
        $request->file('fichero')->move($path, $nombre_fichero);


        $socio = Socio::find($id);
        $socio->nombre = $request->get('nombre');
        $socio->apellidos = $request->get('apellidos');
        $socio->dni = $request->get('dni');
        $socio->telefono = $request->get('telefono');
        $socio->fecha_de_nacimiento = $request->get('fecha');
        $socio->localidad = $request->get('localidad');
        $socio->provincia = $request->get('provincia');
        $socio->direccion = $request->get('direccion');
        $socio->pais = $request->get('pais');
        $socio->codigo_postal = $request->get('codigop'); 
        $socio->observaciones = $request->get('observaciones');
        $socio->ruta = $nombre_fichero;
        $socio->save();
        return redirect('/socios')->with('success', 'Socio actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $socio = Socio::find($id);
        $socio->delete();
        return redirect('/socios')->with('success', 'Socio eliminado correctamente');
    }
}
